class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.integer :mobile
      t.integer :amount_spent_id
      t.string :bill_number
      t.string :payment_type
      t.datetime :transaction_datetime

      t.timestamps
    end
  end
end
