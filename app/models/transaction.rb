class Transaction < ApplicationRecord

  belongs_to :user, optional: true
  belongs_to :merchant, optional: true, class_name: "Company"
  belongs_to :amount_spent, optional: true, class_name: true
end
