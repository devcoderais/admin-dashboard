class CreateAmounts < ActiveRecord::Migration[5.2]
  def change
    create_table :amounts do |t|
      t.decimal :value, precision: 64, scale: 12
      t.integer :currency_id

      t.timestamps
    end
  end
end
