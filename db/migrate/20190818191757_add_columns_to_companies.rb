class AddColumnsToCompanies < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :display_name, :string
    add_column :companies, :business_name, :string
    add_column :companies, :enabled, :boolean
  end
end
