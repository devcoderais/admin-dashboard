class Amount < ApplicationRecord
  
  belongs_to :currency, optional: true
end
