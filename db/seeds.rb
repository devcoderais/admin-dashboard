# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Currency.create(country: "India", country_code: "IND")

5000.times do |number|
  name = Faker::Company.name + ' ' + number.to_s
  Company.create(display_name: name, business_name: name)
end